FROM ubuntu:18.04

RUN apt update \
    && apt install -y git python3 python3-pip openjdk-8-jre \
    && git clone -b snapshot https://gitlab.com/crafty-controller/crafty-web.git /crafty_web \
    && cd /crafty_web \
    && rm -rf .git/ \
    && pip3 install --no-cache-dir -r requirements.txt \
    && mkdir /crafty_db /crafty_config \
    && apt purge -y git \
    && apt autoremove -y

COPY crafty-config.yml /crafty_config/config.yml

EXPOSE 8000
EXPOSE 25500-25600

WORKDIR /crafty_web

CMD ["python3", "crafty.py", "-c", "/crafty_config/config.yml"]
